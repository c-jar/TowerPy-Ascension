#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import os

DOSSIER_COURRANT = os.path.dirname(os.path.abspath(__file__))
DOSSIER_PARENT = os.path.dirname(os.path.dirname(DOSSIER_COURRANT))
sys.path.append(DOSSIER_PARENT)
import pygame
from animation import Animation
from PodSixNet.Connection import ConnectionListener
from config.config import Config


class Joueur(pygame.sprite.Sprite, ConnectionListener):
    def __init__(self, id, centerx, centery, vie):
        pygame.sprite.Sprite.__init__(self)
        self.image, self.rect = Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_immobile/joueur_immobile_gauche.png'))
        self.animation = Animation(5)

        # Animation arrêt
        self.animation.addImage(("arret", "gauche"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_immobile/joueur_immobile_gauche.png'))[0])
        self.animation.addImage(("arret", "droite"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_immobile/joueur_immobile_droite.png'))[0])

        # Animation de la marche vers la gauche
        self.animation.addImage(("marche", "gauche"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_gauche/joueur_gauche1.png'))[0])
        self.animation.addImage(("marche", "gauche"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_gauche/joueur_gauche2.png'))[0])
        self.animation.addImage(("marche", "gauche"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_gauche/joueur_gauche3.png'))[0])
        self.animation.addImage(("marche", "gauche"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_gauche/joueur_gauche4.png'))[0])

        # Animation de la marche vers la droite
        self.animation.addImage(("marche", "droite"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_droite/joueur_droite1.png'))[0])
        self.animation.addImage(("marche", "droite"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_droite/joueur_droite2.png'))[0])
        self.animation.addImage(("marche", "droite"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_droite/joueur_droite3.png'))[0])
        self.animation.addImage(("marche", "droite"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_droite/joueur_droite4.png'))[0])

        # Animation de saut
        self.animation.addImage(("saut", "gauche"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_saut/joueur_saut_gauche.png'))[0])
        self.animation.addImage(("saut", "droite"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_saut/joueur_saut_droite.png'))[0])

        # Animation de chute
        self.animation.addImage(("chute", "gauche"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_chute/joueur_chute_gauche.png'))[0])
        self.animation.addImage(("chute", "droite"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_chute/joueur_chute_droite.png'))[0])

        # Animation de tir
        self.animation.addImage(("tir", "droite"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_tir/joueur_tir_droite1.png'))[0])
        self.animation.addImage(("tir", "gauche"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_tir/joueur_tir_gauche1.png'))[0])

        # Animation de l'attaque au corps à corps
        self.animation.addImage(("cac", "droite"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_cac/joueur_cac_droite1.png'))[0])
        self.animation.addImage(("cac", "droite"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_cac/joueur_cac_droite2.png'))[0])
        self.animation.addImage(("cac", "gauche"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_cac/joueur_cac_gauche1.png'))[0])
        self.animation.addImage(("cac", "gauche"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_cac/joueur_cac_gauche2.png'))[0])

        # Animation lors de la mort du personnage
        self.animation.addImage(("mort", "gauche"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_mort/joueur_mort_gauche1.png'))[0])
        self.animation.addImage(("mort", "gauche"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_mort/joueur_mort_gauche2.png'))[0])
        self.animation.addImage(("mort", "gauche"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_mort/joueur_mort_gauche3.png'))[0])
        self.animation.addImage(("mort", "gauche"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_mort/joueur_mort_gauche4.png'))[0])

        self.animation.addImage(("mort", "droite"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_mort/joueur_mort_droite1.png'))[0])
        self.animation.addImage(("mort", "droite"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_mort/joueur_mort_droite2.png'))[0])
        self.animation.addImage(("mort", "droite"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_mort/joueur_mort_droite3.png'))[0])
        self.animation.addImage(("mort", "droite"), Config.load_png(
            os.path.join(DOSSIER_PARENT, 'sprites/joueur/joueur_mort/joueur_mort_droite4.png'))[0])

        # Variables
        self.action = ("marche", "gauche")
        self.sound = {}
        self.sound["tir"] = pygame.mixer.Sound(os.path.join(DOSSIER_PARENT, 'sound/arc.mp3'))

        self.rect.centerx = centerx
        self.rect.centery = centery
        self.rect.size = (24, 38)
        self.id = id
        self.vie = vie

    def updateImage(self):
        image = self.animation.getImage(self.action)
        if image != None:
            self.image = image

    def playSound(self):
        if self.sound.has_key(self.action[0]) and self.action[0] != self.sound["encour"]:
            self.sound[self.action[0]].stop()
            self.sound[self.action[0]].play()
            self.sound["encour"] = self.action[0]
        else:
            self.sound["encour"] = self.action[0]

class GroupJoueurs(pygame.sprite.RenderClear, ConnectionListener):
    def __init__(self):
        pygame.sprite.RenderClear.__init__(self)

    def init(self, data):
        """Recupère les joueurs"""
        self.empty()

        list = data["players"]
        for k in list:
            self.add(Joueur(k["id"], k["rect"][0], k["rect"][1], k["vie"]))
        print data["players"]

    def update_players(self, data):
        """Recupère les joueurs"""
        list = data["players"]
        sprites = self.sprites()
        for sprite in sprites:
            for player in list:
                if sprite.id == player["id"]:
                    sprite.rect.centerx = player["rect"][0]
                    sprite.rect.centery = player["rect"][1]
                    sprite.vie = player["vie"]
                    if player["action"] != None:
                        sprite.action = player["action"]

    def Network_players(self, data):
        """Recupère les joueurs"""
        if self.sprites().__len__() == 0:  # Pour le premier envoie
            #On créer des sprites représentant les joueurs
            self.init(data)
        else:  # sinon, il suffit de mettre à jour les postion des sprites existant
            self.update_players(data)

        # On update les inamations
        sprites = self.sprites()
        for sprite in sprites:
            sprite.updateImage()
            # sprite.playSound()

    def Network_killPlayer(self, data):
        """Supprime un joueur"""
        for sprite in self.sprites():
            if sprite.id == data["idPlayer"]:
                sprite.kill()

    def getJoueurById(self, id):
        for s in self.sprites():
            if s.id == id:
                return s

        return None

    def update(self):
        self.Pump()
