# coding=utf-8
import sys, pygame
from PodSixNet.Server import Server
from pygame.locals import *
from config.config import Config

DF_SPEED_SHOT = 5

class Shot(pygame.sprite.Sprite):

    def __init__(self, x, y, speed, direction):
        pygame.sprite.Sprite.__init__(self)
        self.rect = pygame.sprite.Rect((0,0),(8,8))
        self.speed = (DF_SPEED_SHOT, 0)
        self.rect.center = [x, y]
        self.speed = speed
        self.direction = direction
        self.time = 0

    def update(self):
        if self.rect.centery > Config.SCREEN_HEIGHT-1:
            self.rect.centery = 1
        if self.rect.centery < 1: # Si le joueur sort de l'écran par le haut
            self.rect.centery = Config.SCREEN_HEIGHT-1
        if self.rect.centerx < 1:
            self.rect.centerx = Config.SCREEN_WIDTH-1
        if self.rect.centerx > Config.SCREEN_WIDTH-1:
            self.rect.centerx = 1

        self.rect = self.rect.move(self.speed)

        self.time += 1

        if self.time >= 10:
            self.speed[1] = 1
