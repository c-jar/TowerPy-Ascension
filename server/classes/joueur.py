# coding:utf-8
import pygame
from config.config import Config
from server.classes.shot import Shot

DF_SPEED_RUN = 5 # vitesse droite / gauche par défaut
DF_SPEED_JUMP = 5 # vitesse haut / bas par défaut
DF_TIME_ATTACK = 20 # Temps de l'attaque de corps à corps
DF_COULD_TIR = 20

class Joueur(pygame.sprite.Sprite):
    def __init__(self, id):
        pygame.sprite.Sprite.__init__(self)
        self.rect = pygame.sprite.Rect((0,0),(22, 34))
        self.spawn = [Config.SCREEN_WIDTH / 2, Config.SCREEN_HEIGHT / 2]
        self.rect.center = self.spawn
        self.speed = [0, DF_SPEED_JUMP]
        self.aTerre = False
        self.couldWallJumpRight = False
        self.couldWallJumpLeft = False
        self.couldAttack = 0
        self.joueurToucher = None # Pour eviter de toucher un joueur à l'infini
        self.couldTire = 0
        self.id = id
        self.vie = 3
        self.action = ("arret", "gauche")
        self.group_shot = pygame.sprite.RenderClear() #Groupe des sprite de tir.


    def init(self):
        self.speed = [0, DF_SPEED_JUMP]
        self.aTerre = False
        self.couldWallJumpRight = False
        self.couldWallJumpLeft = False
        self.couldAttack = 0
        self.joueurToucher = None # Pour eviter de toucher un joueur à l'infini
        self.couldTire = 0
        self.vie = 3
        self.action = ("arret", "gauche")
        self.group_shot.empty()

    def jump(self):
        if self.action[0] != "action" and self.aTerre \
            or (self.couldWallJumpRight and self.action[1] == "droite") \
            or (self.couldWallJumpLeft and self.action[1] == "gauche"):
            self.aTerre = False
            self.speed[1] = -DF_SPEED_JUMP
            self.couldWallJumpLeft = False
            self.couldWallJumpRight = False
            self.action = ("saut", self.action[1])

        self.rect.size = (20,34)

    def down(self):
        self.speed[1] = DF_SPEED_JUMP

    def left(self):
        self.speed[0] = -DF_SPEED_RUN
        if self.action[0] == "arret":
            self.action = ("marche", "gauche")
        else:
            self.action = (self.action[0], "gauche")

        self.rect.size = (22, 34)

    def right(self):
        self.speed[0] = DF_SPEED_RUN
        if self.action[0] == "arret":
            self.action = ("marche", "droite")
        else:
            self.action = (self.action[0], "droite")

        self.rect.size = (22, 34)

    def stopJumping(self):
        self.speed[1] = 0
        self.action = ("chute", self.action[1])
        self.rect.size = (20, 40)

    def stopY(self):
        self.speed[1] = 0

    def stopX(self):
        self.speed[0] = 0

    def shoot(self):
        if self.couldTire == 0:
            if self.action[1] == "droite":
                speedX = 8
                self.action = ("tir", "droite")
            else:
                speedX = -8
                self.action = ("tir", "gauche")

            self.rect.size = (22, 34)
            shot = Shot(self.rect.centerx, self.rect.centery, [speedX, 0], self.action[1])
            self.group_shot.add(shot)
            self.couldTire = DF_COULD_TIR

    def attaqueCorpACorp(self):
        if self.couldAttack == 0:
            self.action = ("cac", self.action[1])
            self.rect.size = (24, 34)
            self.couldAttack = DF_TIME_ATTACK
            self.joueurToucher = None

    def dead(self):
        """Enlève une vie"""
        self.vie = self.vie - 1
        print(self.vie)
        #self.action = ("mort", self.action[1])
        return self.vie

    def setSpawn(self, (x, y)):
        self.spawn = (x,y)
        self.rect.center = self.spawn

    def reSpawn(self):
        self.rect.center = self.spawn

    def update(self):
        positionChange = False

        if self.rect.centery > Config.SCREEN_HEIGHT-1:
            self.rect.centery = 1
            positionChange = True
        if self.rect.centery < 1: # Si le joueur sort de l'écran par le haut
            self.rect.centery = Config.SCREEN_HEIGHT-1
            positionChange = True
        if self.rect.centerx < 1:
            self.rect.centerx = Config.SCREEN_WIDTH-1
            positionChange = True
        if self.rect.centerx > Config.SCREEN_WIDTH-1:
            self.rect.centerx = 1
            positionChange = True

        if self.couldAttack > 0:
            self.couldAttack = self.couldAttack -1
            positionChange = True
            if self.couldAttack <= (DF_TIME_ATTACK - 5):
                self.action = ("arret", self.action[1])

        if self.couldTire > 0:
            self.couldTire -= 1
            positionChange = True
            if self.couldTire == 0:
                self.action = ("arret", self.action[1])

        # Mise en jour de la vitesse de saut
        # S'il ne monte plus
        if (self.action[0]=="saut" and self.speed[1] >= 0) or (not self.aTerre and self.action[0]!="saut"):
            if self.action[0] != "tir":
               # self.jumping = False
                self.action = ("chute", self.action[1])
                positionChange = True

        # S'il n'a pas atteint sa vitesse max vers le bas
        if self.speed[1] < DF_SPEED_JUMP and not self.aTerre:
            self.speed[1] += 0.15
        elif not self.aTerre:
            self.speed[1] = DF_SPEED_JUMP
        else:
            self.speed[1] = 0


        # Mise à jour de la position du joueur
        old_rect = self.rect.copy()
        self.rect = self.rect.move(self.speed)

        # Si sa position a changé
        if old_rect != self.rect or self.couldAttack > 0:
            positionChange = True
        elif self.action[0] != "arret" and self.aTerre and self.action[0] != "tir" and self.action[0] != "cac": # Si le joueur est arreter
            self.action = ("arret", self.action[1])
            positionChange = True

        self.speed[0] = 0

        self.group_shot.update()

        #print str(positionChange) + " " + str(old_rect) + " " + str(self.rect) + " " + str(self.couldAttack) + " " + str(self.aTerre)
        return positionChange


