# coding=utf-8
import os, sys
DOSSIER_COURRANT = os.path.dirname(os.path.abspath(__file__))
DOSSIER_PARENT = os.path.dirname(DOSSIER_COURRANT)
sys.path.append(DOSSIER_PARENT)
from PodSixNet.Channel import Channel
from pygame.locals import *
from classes.joueur import Joueur

class ClientChannel(Channel):
    def __init__(self, *args, **kwargs):
        Channel.__init__(self, *args, **kwargs)
        self.joueur = Joueur(self._server.get_new_id())
        self.proposeMap = None
        self.ready = False

        self.Send({"action":"id", "id":self.joueur.id})

    def Close(self):
        self._server.del_client(self)

    #def Network(self, data):
    #    print('message de type %s recu' % data['action'])

    def Network_ready(self, data):
        self.ready = True
        self.proposeMap = data["map"]
        print "Client propose la map " + str(self.proposeMap)

    def Network_touches(self, data):
        """Reçoie les messages de types touches et agit en conséquence"""
        if self.joueur.vie >= 0 and self.ready:
            touches = data["touches"]
            if(touches[K_SPACE]):
                #Jump
                self.joueur.jump()
            if touches[K_LEFT]:
                #left
                self.joueur.left()
            if touches[K_RIGHT]:
                #right
                self.joueur.right()

            if touches[K_s]:
                #tir
                self.joueur.shoot()

            if touches[K_f]:
                self.joueur.attaqueCorpACorp()


    def update(self):
        """Fonction qui met à jour tous les objets liés au clients"""
        result = self.joueur.update()
        return result

    def reinitPlayer(self):
        self.joueur.init()
        self.ready = False
